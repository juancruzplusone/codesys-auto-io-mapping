import csv
import json
import os
from scriptengine import *


def export_io(fieldbus_name, file_path):
    """Export device IO mapping to CSV."""
    device_list = projects.primary.find(fieldbus_name, True)
    if not device_list:
        print("Device not found!")
        exit()

    # CODESYS find call returns a list of devices, but we only want the first one
    device = device_list[0]

    # Create mapping CSV file for current device
    io_csv_path = os.path.join(file_path, "io_mappings.csv")
    device.export_io_mappings_as_csv(io_csv_path)
    print("CONSOLE: IO Mapping saved to {}".format(io_csv_path))

    return io_csv_path


def load_config_files(file_path, io_csv_path):
    """Load the JSON part IO mapping database, user defined IO configuration, and IO mappings CSV."""
    with open(os.path.join(file_path, 'component_database.json'), 'r') as f:
        database = json.load(f)

    with open(os.path.join(file_path, 'IO_config.csv'), 'r') as f:
        reader = csv.reader(f)
        next(reader)  # Skip the header
        io_config = list(reader)

    with open(io_csv_path, 'r') as f:
        reader = csv.reader(f)
        current_io_mappings = list(reader)

    print("CONSOLE: Loaded configuration files")

    return database, io_config, current_io_mappings


def map_io(database, io_config, io_mappings):
    """
    Map IO based on the configuration and database:

    The function iterates through the user-defined IO configuration. For each row, it finds the corresponding part in the 
    database and retrieves its input/output mappings. It then iterates through the current IO mappings and updates the 
    mappings for the corresponding channel and mapping type (input or output). If a part has no mappings for inputs/outputs, 
    it is handled by initializing an empty list for the mappings.
    """
    
    # Create a list of the current mapping CSV
    updated_io_mappings = list(io_mappings)

    # Iterate through User specified IO configuration and user row data to create mapping in IO_csv
    for row in io_config:
        channel, object_name, part_number = row
        mappingList = None  # Initialize mapping to None
        mappingType = None  # Initialize mapping type to None

        # Iterate through the database to find the part number
        for part in database:
            if part['PartNumber'] == part_number:
                if part['OutputMapping'] is not None:
                    mappingList = part['OutputMapping']
                    mappingType = 'Output'
                elif part['InputMapping'] is not None:
                    mappingList = part['InputMapping']
                    mappingType = 'Input'
                break

        # Handle case where part has no mapping for In/outputs
        if mappingList is None:
            mappingList = []

        # Iterate through the current IO mappings and update the mappingList
        mapping_index = 0
        for i, row in enumerate(updated_io_mappings):
            if len(row) > 1 and 'IO-Link Port {} - {} Data'.format(channel, mappingType) in row[1]:
                if mapping_index < len(mappingList):
                    updated_io_mappings[i][0] = 'Application.PLC_PRG.{}.{}'.format(object_name, mappingList[mapping_index])
                    mapping_index += 1

        print("CONSOLE: {} {} mapped to {}".format(part_number, mappingType, object_name))

    print("CONSOLE: IO Mappings updated")

    return updated_io_mappings



def save_io_mappings(io_csv_path, updated_io_mappings):
    """Save the updated io_mappings to a CSV file."""
    with open(io_csv_path, 'wb') as f:  # Open file in text mode
        writer = csv.writer(f)
        writer.writerows(updated_io_mappings)

    print("CONSOLE: IO Mappings saved to {}".format(io_csv_path))


if __name__ == "__main__":
    print("CONSOLE: Starting IO Mapping Script")

    fieldbus_name = 'IMPACT67_Pro_E_DIO8_IOL8_5P'  # Device name
    file_path = r"C:\Users\JuanCarlosCruz\OneDrive - Plus One Robotics\CODESYS Docs\codesys-auto-io-mapping"

    io_csv_path = export_io(fieldbus_name, file_path)
    database, io_config, io_mappings_to_update = load_config_files(file_path, io_csv_path)
    updated_io_mappings = map_io(database, io_config, io_mappings_to_update)
    save_io_mappings(io_csv_path, updated_io_mappings)
