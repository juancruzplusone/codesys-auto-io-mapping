# CODESYS EthernetIP IOLink Master Fieldbus Library

This library is designed to simplify the process of mapping IO fields for EthernetIP IOLink master fieldbuses in CODESYS. It automatically generates a mapping CSV and provides hardware device abstractions, allowing users to call general objects in their code without worrying about specific part numbers.

## Library Usage

To use the library, follow these steps:

1. Create a `IO_config.csv` in your project's active directory. See the [Example](#example-io_config.csv) for reference.
2. Write your code in the PLC_PRG block.
3. Run the Python script. The script will automatically find the device in your CODESYS project, export its IO mappings to a CSV file, load the necessary configuration files, update the IO mappings based on your configuration and the part database, and save the updated IO mappings back to the CSV file.


## Example IO_config.csv

Here is an example of what your "IO_config.csv" file might look like:
```
Channel,ObjectName,PartNumber
1,Device1,123456
2,Device2,789012
3,Device3,345678
```
In this example, the script will map the IO fields for three devices with the part numbers 123456, 789012, and 345678 on channels 1, 2, and 3, respectively.
Note

## How it Works

The library uses a Python script and a JSON part database to automate the IO mapping process. The user provides an "IO_config.csv". The script will map all relevant IO fields based on the user's configuration and the part database.

### The provided Python script performs the following steps:

1.    Export device IO mapping to CSV: The script first finds the device in the CODESYS project and exports its IO mappings to a CSV file.
2.   Load configuration files: The script then loads three configuration files: the JSON part database, the user-defined IO configuration CSV, and the newly created IO mappings CSV.
3.   Map IO: The script iterates through the user's IO configuration and the part database to update the IO mappings CSV. It currently only supports output mappings, but future updates may include support for input mappings.
4.   Save updated IO mappings: Finally, the script saves the updated IO mappings back to the CSV file.

## Limitations

Database is currently limited to the components I have on hand for testing.
